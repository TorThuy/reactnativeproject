import React, { Component } from 'react'
import { View, Text, Button } from 'react-native'
import Headers from '../../components/Header/index'


const arrayData = {
    "title": "YEAH",
    "resuletCode": 20000,
    "description": "Success",
    "data": [
        {
            "data1": "this is data",
            "data2": "Same"
        },
        {
            "data1": "this is data2",
            "data2": "Same2"
        }
    ]
}
export default class HomeScreen extends Component {

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Headers textHeader="Home"/>
                <Text>Home Screen</Text>
              
                <Button
                    title="Go to Details"
                    onPress={this.onClickGotoDeatailScreen}
                />
            </View>
        )
    }


    onClickGotoDeatailScreen = () => {
        this.props.navigation.navigate('Details', { message: "Hello I'm from Home Screen",data : arrayData })
    }

}



