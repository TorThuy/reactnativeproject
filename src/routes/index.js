import React,{Component} from "react";
import { createStackNavigator } from "react-navigation";
import HomeScreen from '../screen/Home/index'
import DetailsScreen from '../screen/Details/index'

const RootStack = createStackNavigator({
    Home: HomeScreen,
    Details :DetailsScreen
    },{
        initialRouteName: 'Home',
    })

export default class App extends Component {
    render() {
        return <RootStack/>
    }
}
