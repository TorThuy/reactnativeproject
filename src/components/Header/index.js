import React,{Component} from 'react'
import {Text,View,ImageBackground} from'react-native'
import  style from './style'

export default class Headers extends Component{

    render(){
        return(
          <View>
              <Text>
                  {this.props.textHeader}
              </Text>
          </View>
        )
    }
}
