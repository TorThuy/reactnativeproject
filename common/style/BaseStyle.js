import {StyleSheet, Dimensions, Platform} from 'react-native'


export default createStyles = (style ={}) => {
    return StyleSheet.create({...style})
}